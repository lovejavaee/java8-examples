package cluodev.github.com.java8.recursive;

import java.util.Arrays;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import static cluodev.github.com.java8.recursive.Substring11Counter.count;
import static cluodev.github.com.java8.recursive.Substring11Counter.count2;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

@RunWith(Parameterized.class)
public class Substring11CounterTest {

    private String input;
    private int count;

    @Parameterized.Parameters (name = "{index}: Substring11Counter.count ({0})={1}")
    public static Iterable<Object[]> data() {
        return Arrays.asList(new Object[][]{
                {"", 0},{"abc",0},{"ab11c", 1}, {"ab11c11", 2}, {"ab11c11mm11xx", 3},
                {"ab111c", 1}
        });
    }

    public Substring11CounterTest(String input, int count) {
        this.input = input;
        this.count = count;
    }


    @Test
    public void count_count11substrings() throws Exception {
       assertThat("", count(input), is(count));
    }

    @Test
    public void count2_count11substrings() throws Exception {
       assertThat("", count2(input), is(count));
    }
}