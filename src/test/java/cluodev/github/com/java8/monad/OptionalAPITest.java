package cluodev.github.com.java8.monad;

import java.time.LocalDate;
import java.time.Month;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import org.hamcrest.CoreMatchers;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertThat;

public class OptionalAPITest {

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Test
    public void testOptional() throws Exception {

        String str1 = null;
        Optional<String> nullableString = Optional.ofNullable(str1);

        if (nullableString.isPresent()) {
            Assert.fail("failed");
        }
    }

    @Test
    public void sortedWithFindFirst() throws Exception {
        LocalDate date1 = LocalDate.of(2016, Month.APRIL,12);
        LocalDate date2 = LocalDate.of(2016, Month.APRIL,10);
        LocalDate date3 = LocalDate.of(2016, Month.APRIL,13);
        List<LocalDate> dates = Arrays.asList(date1, date3, date2);
        LocalDate latestDate = getLatestDate(dates).get();

        assertThat(latestDate).as("Latest date was expected").isEqualTo(date2);

        getLatestDate(dates).ifPresent(
             x-> System.out.print(x.toString())
        );

    }

    @Test
    public void sortedWithFindFirst_emptyList() throws Exception {
        List<LocalDate> emptyList = Collections.emptyList();
        final LocalDate aDate = LocalDate.of(2017,Month.AUGUST,18);

        Optional<LocalDate> latestDate = getLatestDate(emptyList);
        assertThat("Optional with empty value is expected", latestDate.isPresent(), CoreMatchers.is(false));

        LocalDate orElseDate = latestDate.orElseGet(
                ()-> aDate
        );
        assertThat("Optional with empty value is expected", orElseDate, CoreMatchers.is(aDate));

        try {
            latestDate.orElseThrow(()-> new IllegalStateException("optional date is empty !"));
        }catch(RuntimeException e){
            System.out.printf("Caught exception: %s\nmessage: %s",e.getClass().getName(), e.getMessage() );
        }
    }


    private Optional<LocalDate> getLatestDate(List<LocalDate> dates) {
        return dates.stream()
                .sorted((e1, e2) -> e2.compareTo(e1))
                .findFirst();

    }
}