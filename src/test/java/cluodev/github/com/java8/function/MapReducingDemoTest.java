package cluodev.github.com.java8.function;

import java.util.Arrays;
import java.util.List;
import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 */
public class MapReducingDemoTest {

    private MapReducingDemo mapReducingDemo;

    @Before
    public void beforeEachTest() throws Exception {
          mapReducingDemo = new MapReducingDemo();
    }

    @Test
    public void listDistinctCharInAString_helloWorld() throws Exception {
       final char[] chars =   mapReducingDemo.listDistinctChars("Hello World");
        List<Character> expectedChars = Arrays.asList('H', 'e', 'l', 'o', 'W', 'r', 'd');

        Assertions.assertThat(chars).isEqualTo(expectedChars);
    }

}