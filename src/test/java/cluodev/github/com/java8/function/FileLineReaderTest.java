package cluodev.github.com.java8.function;


import org.junit.Assert;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;

/**
 */
public class FileLineReaderTest {

    @Test
    public void oneLineIsRead() throws Exception {
      String lineOne =  FileLineReader.readOneLine("src/test/resources/unittest/test.txt");
        Assert.assertThat("line one was not expected",lineOne, is("abc"));
    }
    
    @Test
    public void twoLinesAreRead() throws Exception {
        String twoLines = FileLineReader.readTwoTLines("src/test/resources/unittest/test2.txt");
        Assert.assertThat("two lines were expected", twoLines, is("abc\nmnp"));
    }
}