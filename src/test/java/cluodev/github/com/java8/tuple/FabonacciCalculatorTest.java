package cluodev.github.com.java8.tuple;


import java.util.Arrays;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;


@RunWith(Parameterized.class)
public class FabonacciCalculatorTest {

    private int numberOfNumbersToPrint;
    private String output;

    @Parameterized.Parameters(name = "{index}: Fabonacci({0})={1}")
    public static Iterable<Object[]> data() {
        return Arrays
                .asList(new Object[][] { { 1, "1" }, { 2, "1,1"}, { 5, "1,1,2,3,5" },
                            {9,"1,1,2,3,5,8,13,21,34"}
                        });
    }

    public FabonacciCalculatorTest(int numberOfNumbersToPrint, String output) {

        this.numberOfNumbersToPrint = numberOfNumbersToPrint;
        this.output = output;
    }

    @Test
    public void printN_numberOf_fabonacci_numbers() throws Exception {
        FabonacciCalculator fabonacciCalculator = new FabonacciCalculator();
                Assert.assertEquals("",output,fabonacciCalculator.print(numberOfNumbersToPrint));
    }
}