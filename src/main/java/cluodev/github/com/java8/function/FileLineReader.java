package cluodev.github.com.java8.function;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class FileLineReader {
    private transient static final Logger LOG = LoggerFactory.getLogger(FileLineReader.class);


    public static String readOneLine(String fileName) throws IOException {
       return  processFile(fileName,
                reader-> reader.readLine()
        );
    }

    private static String processFile(final String file, BufferedReaderFileProcessor processor) throws IOException {
        try (BufferedReader reader = new BufferedReader(new FileReader(file))){
           return processor.process(reader);
        }
    }

    public static String readTwoTLines(String fileName) throws IOException {
       return processFile(fileName,
                reader->reader.readLine()+"\n"+ reader.readLine());
    }


    @FunctionalInterface
    public interface BufferedReaderFileProcessor{
        String process(BufferedReader reader) throws IOException;
    }
}
