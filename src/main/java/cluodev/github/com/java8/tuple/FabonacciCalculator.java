package cluodev.github.com.java8.tuple;

import java.util.stream.Stream;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class FabonacciCalculator {
    private transient static final Logger LOG = LoggerFactory.getLogger(FabonacciCalculator.class);


    public String print(int numberOfNumbersToPrint) {
        final StringBuilder outPut = new StringBuilder();
        Stream.iterate(new long[] { 1, 1 }, p -> new long[] { p[1], p[0] + p[1] })
                .limit(numberOfNumbersToPrint)
                .forEach(p -> outPut.append(p[0]).append(","));

        String strEndsWithComma = outPut.toString();
        return strEndsWithComma.substring(0,strEndsWithComma.lastIndexOf(","));
    }
}
