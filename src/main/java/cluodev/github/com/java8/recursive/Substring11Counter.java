package cluodev.github.com.java8.recursive;

import java.util.Arrays;
import org.apache.commons.lang.NumberUtils;

public class Substring11Counter {


    public static int count(String input) {
        final int len = input.length();
        if (len==0) return 0;

        final int indexOfFirstAppearance = input.indexOf("11");

        if (indexOfFirstAppearance>-1) {
            return 1+count(input.substring(indexOfFirstAppearance+2));
        }

        return 0;
    }

    public static int count2(String input) {
        Arrays.asList(input.toCharArray()).stream()
                 .map(_char->
                     NumberUtils.isNumber(String.valueOf(_char)))
                 .forEach(System.out::println);


        return 0;
    }
}
